package com.tsc.jarinchekhina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.jarinchekhina.tm.command.AbstractDataCommand;
import com.tsc.jarinchekhina.tm.dto.Domain;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-faster-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load JSON data from file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD JSON DATA]");
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
