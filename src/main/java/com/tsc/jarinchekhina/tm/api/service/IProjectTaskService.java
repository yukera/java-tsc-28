package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public interface IProjectTaskService {

    void clearProjects(@Nullable String userId);

    @NotNull
    List<Task> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task bindTaskByProjectId(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Task unbindTaskByProjectId(@Nullable String userId, @Nullable String taskId);

    @NotNull
    Optional<Project> removeProjectById(@Nullable String userId, @Nullable String projectId);

}
